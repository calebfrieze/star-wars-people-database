FROM mysql:latest

ENV MYSQL_DATABASE=sw_app_db
ENV MYSQL_ROOT_PASSWORD=root

COPY ./scripts/ /docker-entrypoint-initdb.d
