USE sw_app_db;

CREATE TABLE people
(
    person_id     INT NOT NULL AUTO_INCREMENT,
    name          VARCHAR(200) NOT NULL,
    height        VARCHAR(20),
    mass          VARCHAR(20) NOT NULL,
    hair_color    VARCHAR(25),
    skin_color    VARCHAR(25),
    eye_color     VARCHAR(25),
    birth_year    VARCHAR(25),
    gender        VARCHAR(20),
    created       DATETIME NOT NULL DEFAULT (UTC_TIMESTAMP()),
    updated       DATETIME NOT NULL DEFAULT (UTC_TIMESTAMP()),

    PRIMARY KEY (person_id)

) ENGINE=InnoDB CHARACTER SET utf8;
